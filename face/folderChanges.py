import os
import time
from datetime import datetime as dt
import requests

path_to_watch = "./fotos"
before = dict([(f, None) for f in os.listdir(path_to_watch)])
while 1:
  time.sleep(10)
  after = dict([(f, None) for f in os.listdir(path_to_watch)])
  added = [f for f in after if not f in before]
  
  if added:
    print ("Added: ", ", ".join(added))
    dirs = os.listdir(path_to_watch)
    #REMOVE OLD FILES
    for file in dirs:
      fileexplode = file.split(":")
      if len(fileexplode) > 1:
        if fileexplode[1] != str(dt.now().minute):
          os.remove(path_to_watch+"/"+file)
    #Iterate added files
    for addedFile in added:
      r = requests.post("http://localhost:9000/",  data={'link': "http://localhost:8000/"+addedFile})
      print(r)
      print("http://localhost:8000/"+addedFile)
  before = after
